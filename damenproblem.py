
import cProfile, pstats

import numpy as np

import random
import time
from typing import List, Tuple
import matplotlib.pyplot as plt

class EightQueensState:

    """This class represents a board in the eight queens puzzle"""
    def __init__(self, state=None, n=8):
        """
        :param state: pass in a numpy array of integers to set the state, otherwise will be generated randomly
        :param n: only used if state is not provided, determines size of board (default: 8)
        """
        if state is None:
            self.n = n
            # state = np.random.randint(0, n, n)
            state = np.random.permutation([_ for _ in range(n)])
        else:
            self.n = len(state)
        self.state = state

    def mutate(self, prob):
        for chromosome in range(self.n):
            if random.random() < prob:
                self.state[chromosome] = np.random.randint(0, self.n)
                
    def mutate_2(self, prob):
        if random.random() < prob:
            # switch position of two chromosomes (state stays a permutation)
            pos_1 = np.random.randint(0, self.n)
            pos_2 = np.random.randint(0, self.n)
            self.state[pos_1], self.state[pos_2] = self.state[pos_2], self.state[pos_1]

    def cost(self):
        """Calculates the number of pairs attacking"""
        count = 0
        for i in range(len(self.state) - 1):
            # for each queen, look in columns to the right
            # add one to the count if there is another queen in the same row
            count += (self.state[i] == np.array(self.state[i + 1:])).sum()

            # add one to the count for each queen on the upper or lower diagonal
            upper_diagonal = self.state[i] + np.arange(1, self.n - i)
            lower_diagonal = self.state[i] - np.arange(1, self.n - i)
            count += (np.array(self.state[i + 1:]) == upper_diagonal).sum()
            count += (np.array(self.state[i + 1:]) == lower_diagonal).sum()
        return count
    
    def is_goal(self):
        return self.cost() == 0

    def __repr__(self) -> str:
        return self.__str__()

    def __str__(self):
        if self.is_goal():
            return f"Goal state! {self.state}"
        else:
            return f"{self.state} cost {self.cost()}"

    def visualize(self):
        for row in range(self.n):
            for column in range(self.n):
                if self.state[row] == column:
                    print("x", end=" ")
                else:
                    print("o", end=" ")
            print("")

def get_goal(population):
    for i in population:
        if i.is_goal():
            return i
    return None

def Random_Algorithm(chromosome_count=8, max_tries=2000):
    for i in range(max_tries):
        current_try = EightQueensState(state=None, n=chromosome_count)
        # print(current_try)
        if current_try.is_goal():
            print(f'Found solution after {i} tried: ')
            print(current_try)
            current_try.visualize()
            return i
    print('Could not find solution in {max_tries} iterations')
    return max_tries

def Genetic_Algorithm(population, target_population_size, best_parents_percentage, mutation_prob, max_epochs=2000):
    epoch = 0
    while not (get_goal(population)):
        new_population = []
        # get best parents
        parents = selection(population, best_parents_percentage)
        epoch+=1
        # print(f"Iteration {i}")
        # print(population[0])
        for _ in range(int(target_population_size/2)):
            children = crossover(parents)
            for child in children:
                child.mutate_2(mutation_prob)
            new_population += children
        population = new_population
        if epoch >= max_epochs:
            print('Could not find solution in {max_epochs} iterations')
            # input()
            break
    else:
        print(f'Found solution after {epoch} epochs: ')
        solution = get_goal(population)
        print(solution)
        solution.visualize()
        
    return population, epoch

def init_agents(population_num, chromosome_count=8):
    return [EightQueensState(state=None, n=chromosome_count) for _ in range(population_num)]

def reproduce(x: EightQueensState, y: EightQueensState):
    """
    Does a split in a random position and concatenates one half of the list 
    with the other half and vice versa. 
    Thus produces and returns two new queen states.
    """
    chromosome_count = x.n
    split = np.random.randint(0, chromosome_count)
    parent_1_first_chromosomes, parent_1_second_chromosomes = x.state[:split], x.state[split:]
    parent_2_first_chromosomes, parent_2_second_chromosomes = y.state[:split], y.state[split:]
    child_1_chromosomes = np.concatenate((parent_1_first_chromosomes, parent_2_second_chromosomes), axis=0)
    child_2_chromosomes = np.concatenate((parent_2_first_chromosomes, parent_1_second_chromosomes), axis=0)
    queen1 = EightQueensState(child_1_chromosomes)
    queen2 = EightQueensState(child_2_chromosomes)
    return [queen1, queen2]
    

def reproduce_2(x: EightQueensState, y: EightQueensState):
    """
    Splits parent A in a random position and uses the first section for the child. 
    The second section is filled by numbers from parent B 
    that are not yet included in the child starting at the split position. 
    This is done twice where each parent is once A and once B. 
    """
    chromosome_count = x.n
    split = np.random.randint(0, chromosome_count)
    parent_1_first_chromosomes, parent_1_second_chromosomes = x.state[:split], x.state[split:]
    parent_2_first_chromosomes, parent_2_second_chromosomes = y.state[:split], y.state[split:]
    child_1_chromosomes = parent_1_first_chromosomes
    child_2_chromosomes = parent_2_first_chromosomes
    for chromosome in np.concatenate((parent_2_second_chromosomes, parent_2_first_chromosomes), axis=0):
        if chromosome not in child_1_chromosomes:
            child_1_chromosomes = np.append(child_1_chromosomes, chromosome)
    for chromosome in np.concatenate((parent_1_second_chromosomes, parent_1_first_chromosomes), axis=0):
        if chromosome not in child_2_chromosomes:
            child_2_chromosomes = np.append(child_2_chromosomes, chromosome)
    
    queen1 = EightQueensState(child_1_chromosomes)
    queen2 = EightQueensState(child_2_chromosomes)
    return [queen1, queen2]

def selection(population, percentage=0.2):
    population = sorted(population, key=lambda population: population.cost())
    population = population[:int(percentage*len(population))]
    return population

def crossover(population):
    x, y = random.sample(population, 2)
    # print("crossover:")
    # print(f'{x=} {y=}')
    # children = reproduce(x, y)
    children = reproduce_2(x, y)
    # print(f'{children=}')
    # input()
    
    return children

def plot_runs(times, title='title'):
    plt.plot(times)
    plt.xlabel('x - run / seed number')
    plt.ylabel('y')
    plt.title(title)
    plt.show()

def statistics(list):
    print(f"Average: {np.average(list)}")
    print(f"StdDev:{np.std(list)}")
    print(f"Variance:{np.var(list)}")
    print(f"Max:{np.max(list)}")
    print(f"Min:{np.min(list)}")


def main():
    board_size = 8
    max_epochs = 2000
    runs = 100
    times = []
    epochs = []
    for i in range(runs):
        random.seed(i)
        np.random.seed(i)
        print("Current seed is:",i,"")
        population = init_agents(population_num=10, chromosome_count=board_size)
        t0 = time.time()
        population, used_epochs = Genetic_Algorithm(population, target_population_size=100, best_parents_percentage=0.3, mutation_prob=0.2, max_epochs=max_epochs)
        # used_epochs = Random_Algorithm(chromosome_count=board_size, max_tries=500000)
        t1 = time.time()
        total = t1-t0
        print(f'time: {total}')
        times.append(total)
        epochs.append(used_epochs)

    print('\nTime: ')
    statistics(times)
    plot_runs(times, 'Used Time')

    print('\nEpochs: ')
    statistics(epochs)
    plot_runs(epochs, 'Used Epochs')


if __name__ == "__main__":
    # profiler = cProfile.Profile()
    # profiler.enable()
    main()
    # profiler.disable()
    # stats = pstats.Stats(profiler).sort_stats('ncalls')
    # stats.print_stats()
    # stats.dump_stats('./stats')

    